package com.ngethe.xmpptest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ngethe.xmpptest.xmpp.XMPPService;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private ChatListAdapter chatListAdapter;
    private boolean boundToXmppService;

    Messenger xmppServiceMessenger;
    final Messenger xmppListenerMessenger = new Messenger(new XMPPListenerHandler());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chatListAdapter = new ChatListAdapter(this);
        ListView lv_chats = findViewById(R.id.lv_chats);
        lv_chats.setAdapter(chatListAdapter);

        bindToXMPPService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindFromXmppService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        chatListAdapter.updateData();
    }

    private void bindToXMPPService() {
        Intent bindToServiceIntent = new Intent(this, XMPPService.class);
        startService(bindToServiceIntent);
        bindService(bindToServiceIntent, xmppServiceConnection, Context.BIND_AUTO_CREATE);
        boundToXmppService = true;
    }

    private void unbindFromXmppService() {
        if (boundToXmppService) {
            if (xmppServiceMessenger != null ) {
                try {
                    Message msg = Message.obtain(null, XMPPService.REMOVE_LISTENER);
                    msg.replyTo = xmppListenerMessenger;
                    xmppServiceMessenger.send(msg);
                } catch (RemoteException ignored) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            unbindService(xmppServiceConnection);
            boundToXmppService = false;
        }
    }

    private ServiceConnection xmppServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            xmppServiceMessenger = new Messenger(service);
            try {
                Message msg = Message.obtain(null, XMPPService.ADD_LISTENER);
                msg.replyTo = xmppListenerMessenger;
                xmppServiceMessenger.send(msg);
                Log.e(TAG, "Connected to service");
            } catch (RemoteException e) {
                Log.e(TAG, "XMPPService crashed before registering listener messenger");
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            xmppServiceMessenger = null;
            Log.e(TAG, "Deregistered listener from XMPP Service");
        }
    };

    class XMPPListenerHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == XMPPService.INCOMING_MESSAGE) {
                handleIncomingMessage();
            }
        }

        void handleIncomingMessage() {
            chatListAdapter.updateData();
        }
    }
}
