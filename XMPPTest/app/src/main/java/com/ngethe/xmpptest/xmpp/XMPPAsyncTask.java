package com.ngethe.xmpptest.xmpp;

import android.os.AsyncTask;
import android.util.Log;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.dns.HostAddress;
import org.jxmpp.jid.EntityBareJid;

import java.net.InetAddress;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class XMPPAsyncTask extends AsyncTask<String, String, String> {
    public static final String TAG = XMPPAsyncTask.class.getSimpleName();
    public static final String HOST = "192.168.1.123";
    public static final String TOAST = "toast";
    public static final String MESSAGE = "message";

    @Override
    protected String doInBackground(String... strings) {
        try {
            InetAddress address = InetAddress.getByName(HOST);
            HostnameVerifier verifier = new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return false;
                }
            };
            XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                    .setHost(HOST)
                    .setUsernameAndPassword("talkie", "talkie")
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                    .allowEmptyOrNullUsernames()
                    .setXmppDomain(HOST)
                    .setHostnameVerifier(verifier)
                    .setHostAddress(address)
                    .setSendPresence(true)
                    .setCompressionEnabled(true)
                    .setPort(5222)
                    .build();
            AbstractXMPPConnection conn1 = new XMPPTCPConnection(config);
            conn1.connect();
            if (conn1.isConnected()) {
                Log.e(TAG, "Connected");
                publishProgress(TOAST,"Connected");
            } else {
                Log.e(TAG, "Connection failed");
                publishProgress(TOAST,"Connection failed");
            }
            conn1.login("talkie", "talkie");
            if (conn1.isAuthenticated()) {
                Log.e(TAG, "Authenticated");
                publishProgress(TOAST,"Authenticated");
                ChatManager chatManager = ChatManager.getInstanceFor(conn1);
                chatManager.addIncomingListener(new IncomingChatMessageListener() {
                    @Override
                    public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
                        if (message != null) {
                            publishProgress(MESSAGE, message.getBody(), message.getFrom().toString());;
                        }
                    }
                });
            } else {
                Log.e(TAG, "Authentication failed");
                publishProgress(TOAST,"Authentication failed");
            }
        } catch (SmackException.ConnectionException e) {
            e.printStackTrace();
            List<HostAddress> hostAddresses= e.getFailedAddresses();
            for (HostAddress hostAddress : hostAddresses) {
                Log.e(TAG, "Failed to connect to " + hostAddress.getFQDN().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            publishProgress(TOAST,e.getMessage());
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
//        Previous implementation in activity
//        TODO: Replace this implementation
//        if (values[0] == TOAST) {
//            Toast.makeText(MainActivity.this, values[1], Toast.LENGTH_LONG).show();
//        } else if (values[0] == MESSAGE) {
//            com.ngethe.xmpptest.models.ChatMessage message = new com.ngethe.xmpptest.models.ChatMessage(values[1], values[2], com.ngethe.xmpptest.models.ChatMessage.RECEIVED);
//            messageListAdapter.addMessage(message);
//        }
    }
}
