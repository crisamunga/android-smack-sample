package com.ngethe.xmpptest.xmpp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.ngethe.xmpptest.models.ChatMessage;
import com.ngethe.xmpptest.models.Database;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.chat2.OutgoingChatMessageListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;
import org.jxmpp.util.XmppStringUtils;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class XMPPService extends Service {
    public static final String TAG = XMPPService.class.getSimpleName();
    public static final String SENDER_NAME = "com.ngethe.xmpptest.extra.sender_name";
    public static final String SENDER_FULL_ADDRESS = "com.ngethe.xmpptest.extra.sender_full_address";
    public static final String MESSAGE = "com.ngethe.xmpptest.extra.message";
    public static final int DISCONNECT = 0;
    public static final int CONNECT = 1;
    public static final int ADD_LISTENER = 2;
    public static final int REMOVE_LISTENER = 3;
    public static final int OUTGOING_MESSAGE = 4;
    public static final int INCOMING_MESSAGE = 5;

    public static final String HOSTNAME = "192.168.1.123";
    public static final String USERNAME = "talkie";
    public static final String PASSWORD = "talkie";

    private Looper xmppServiceLooper;
    private XMPPServiceHandler xmppServiceHandler;
    private AbstractXMPPConnection xmppConnection;
    private ChatManager chatManager;
    private Messenger xmppServiceMessenger;
    private List<Messenger> listeners = new ArrayList<>();
    private Database database;

    public XMPPService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (xmppServiceMessenger != null) {
            return xmppServiceMessenger.getBinder();
        } else {
            return null;
        }
    }

    @Override
    public void onCreate() {
        HandlerThread thread = new HandlerThread("XMPPServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        xmppServiceLooper = thread.getLooper();
        xmppServiceHandler = new XMPPServiceHandler(xmppServiceLooper);
        xmppServiceMessenger = new Messenger(new XMPPServiceHandler(xmppServiceLooper));
        Log.e(TAG, "Service created");
        database = new Database(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message xmppServiceHandlerMessage = xmppServiceHandler.obtainMessage(CONNECT);
        xmppServiceHandlerMessage.arg1 = startId;
        xmppServiceHandler.sendMessage(xmppServiceHandlerMessage);
        Log.e(TAG, "Service start command received");
        return START_STICKY;
    }

    public final class XMPPServiceHandler extends Handler {
        public XMPPServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case CONNECT:
                    handleConnectAction(HOSTNAME, USERNAME, PASSWORD);
                    break;
                case DISCONNECT:
                    handleDisconnectAction();
                    stopSelf(msg.arg1);
                    break;
                case ADD_LISTENER:
                    listeners.add(msg.replyTo);
                    break;
                case REMOVE_LISTENER:
                    listeners.remove(msg.replyTo);
                    break;
                case OUTGOING_MESSAGE:
                    handleOutgoingMessageAction(msg);
                    break;
                default:
                    break;
            }
        }

        private void handleConnectAction(String hostname, String username, String password) {
            HostnameVerifier verifier = new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return false;
                }
            };
            try {
                InetAddress address = InetAddress.getByName(hostname);
                XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                        .setHost(hostname)
                        .setUsernameAndPassword(username, password)
                        .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                        .allowEmptyOrNullUsernames()
                        .setXmppDomain(hostname)
                        .setHostnameVerifier(verifier)
                        .setHostAddress(address)
                        .setSendPresence(true)
                        .setCompressionEnabled(true)
                        .setPort(5222)
                        .build();
                xmppConnection = new XMPPTCPConnection(config);
                xmppConnection.connect();
                if (xmppConnection.isConnected()) {
                    Log.e(TAG, "Connected to XMPP server");
                } else {
                    Log.e(TAG, "Connection failed");
                }
                xmppConnection.login();
                if (xmppConnection.isAuthenticated()) {
                    Log.e(TAG, "Authenticated");
                    chatManager = ChatManager.getInstanceFor(xmppConnection);
                    chatManager.addIncomingListener(xmppIncomingMessageListener);
                    chatManager.addOutgoingListener(xmppOutgoingMessageListener);

                } else {
                    Log.e(TAG, "Authentication failed");
                }
            } catch (Exception e) {
                Log.e(TAG, "Error occurred while connecting to XMPP domain " + hostname);
                e.printStackTrace();
            }
        }

        private void handleDisconnectAction() {
            if (xmppConnection != null) {
                chatManager.removeIncomingListener(xmppIncomingMessageListener);
                chatManager.removeOutgoingListener(xmppOutgoingMessageListener);
                xmppConnection.disconnect();
                Log.e(TAG, "Disconnected from XMPP server");
            }
        }

        private void handleOutgoingMessageAction(Message msg) {
            Bundle bundle = msg.getData();
            String buddy = bundle.getString(XMPPService.SENDER_FULL_ADDRESS);
            String message = bundle.getString(XMPPService.MESSAGE);
            try {
                EntityBareJid buddyJid = JidCreate.entityBareFrom(buddy);;
                Chat chat = chatManager.chatWith(buddyJid);
                chat.send(message);
            } catch (SmackException.NotConnectedException | InterruptedException | XmppStringprepException e) {
                e.printStackTrace();
            }
        }

        private IncomingChatMessageListener xmppIncomingMessageListener = new IncomingChatMessageListener() {
            @Override
            public void newIncomingMessage(EntityBareJid from, org.jivesoftware.smack.packet.Message message, Chat chat) {

                String senderFullAddress = from.toString();
                String senderName = from.getLocalpart().asUnescapedString();
                String messageBody = message.getBody();

                com.ngethe.xmpptest.models.Chat chatRecord = database.getChatsTable().getBySenderFullAddress(senderFullAddress);
                if (chatRecord == null) {
                    chatRecord = new com.ngethe.xmpptest.models.Chat();
                    chatRecord.setSenderFullAddress(senderFullAddress);
                    chatRecord.setSenderName(senderName);
                    chatRecord.addUnreadMessages(1);
                    chatRecord.setModifiedAt(new Date());
                    chatRecord = database.getChatsTable().insert(chatRecord);
                } else {
                    chatRecord.addUnreadMessages(1);
                    chatRecord.setModifiedAt(new Date());
                    database.getChatsTable().update(chatRecord);
                }

                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setMessage(messageBody);
                chatMessage.setBuddy(senderName);
                chatMessage.setType(ChatMessage.RECEIVED);
                chatMessage.setChatId(chatRecord.get_id());
                chatMessage.setCreatedAt(new Date());
                database.getChatMessagesTable().insert(chatMessage);

                for (Messenger listener : listeners) {
                    Message msg = Message.obtain(null, INCOMING_MESSAGE);
                    Bundle bundle = msg.getData();

                    bundle.putString(SENDER_FULL_ADDRESS, senderFullAddress);
                    bundle.putString(SENDER_NAME, senderName);
                    bundle.putString(MESSAGE, messageBody);

                    msg.setData(bundle);
                    try {
                        listener.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        private OutgoingChatMessageListener xmppOutgoingMessageListener = new OutgoingChatMessageListener() {
            @Override
            public void newOutgoingMessage(EntityBareJid to, org.jivesoftware.smack.packet.Message message, Chat chat) {
                Log.e(TAG, "Sent new outgoing message " + message.getBody());
            }
        };
    }
}
